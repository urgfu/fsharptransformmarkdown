﻿#r @"Y:\Development\fsharptransformmarkdown\src\MarkdownTransform\packages\FSharp.Data.2.2.3\lib\net40\FSharp.Data.dll"
#r "System.XML.Linq.dll"
#r @"Y:\Development\FSharpTransformMarkdown\src\MarkdownTransform\packages\FSharp.Formatting.2.9.10\lib\net40\FSharp.Markdown.dll"

open FSharp.Markdown

System.Environment.CurrentDirectory <- @"Y:\Development\fsharptransformmarkdown\src\MarkdownTransform\MarkdownTransform"

open FSharp.Data


//Declare our compiler XML type
type CompilerXML = XmlProvider<"Deedle.xml">

//Take in the XML
let input = CompilerXML.GetSample()

let parseMember (mem :CompilerXML.Member) =
   let mdSummary = Markdown.Parse(mem.Summary.Value)
   let mem = CompilerXML.M

   let rec procSummary processed  = function
    | [] -> processed
    | head:tail
    
   

let members = input.Members |> Seq.map (fun (x :CompilerXML.Member) -> parseMember x) 

members







